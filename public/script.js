function cart() {
    $(".cart-payment").toggleClass("active");
    $(".overlay").toggleClass("active");
}

function addWidth() {
    $("#search").toggleClass("search");
}

function searchClick() {
    $("#searchBtn").click();
}

function orderSubmit() {
    $("#orderBtn").click();
}

function logout() {
    $("#logout-form").submit();
}
