<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\DashboardController::class, 'welcome'])->name('welcome');

Auth::routes();

Route::group(['middleware' => 'auth'],function(){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/product', [App\Http\Controllers\ItemController::class, 'index'])->name('product');
    Route::get('/add-item', [App\Http\Controllers\ItemController::class, 'create'])->name('create');
    Route::post('/add-item', [App\Http\Controllers\ItemController::class, 'store']);

    Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
    Route::get('/category', [App\Http\Controllers\CategoryController::class, 'index'])->name('category');
    Route::post('/category', [App\Http\Controllers\CategoryController::class, 'store']);
    Route::post('/add-to-cart', [App\Http\Controllers\CartController::class, 'store']);
    Route::delete('/remove-cart/{id}', [App\Http\Controllers\CartController::class, 'destroy']);
    Route::post('/checkout', [App\Http\Controllers\CartController::class, 'checkout']);
    Route::put('/withdraw', [App\Http\Controllers\DashboardController::class, 'withdraw']);
});

Route::get('/detail/{id}', [App\Http\Controllers\ItemController::class, 'show']);
