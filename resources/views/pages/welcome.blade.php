@extends('layouts.master')

@section('headers')
<div class="headers">
    <h2 class="text-white">Hello, Anonymous!</h2>
    <p class="text-white-50">{{\Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}</p>
</div>
<div class="authentication">
    <a href="{{route('login')}}" class="text-decoration-none me-3">Login</a>
    <a href="{{route('register')}}" class="text-decoration-none">Register</a>
</div>
@endsection

@section('body')
<div class="categories mt-2 d-flex">
    <span class="ctg me-5">
        <a href="/" class="text-decoration-none">
        <p class="{{ (!$category ? 'blue' : 'text-white-50') }}">Semua</p>
        @if(!$category)
        <div class="bottom"></div>
        @endif
        </a>
    </span>
    @foreach ($categories as $ct)
    <span class="ctg me-5">
        <a href="/?category={{$ct->nama}}" class="text-decoration-none">
        <p class="{{ ($category == $ct->nama ? 'blue' : 'text-white-50') }}">{{$ct->nama}}</p>
        @if($category == $ct->nama)
        <div class="bottom"></div>
        @endif
        </a>
    </span>
    @endforeach
</div>
<div class="orders pe-4">
    <form action="/" method="get" class="d-flex justify-content-between">
        <input type="hidden" name="category" value="{{ $category }}">
        <div class="order-by">
            <p class="text-white mt-2 mb-0">Order By:</p>
            <div class="ord">
                <input type="checkbox" name="waktu" id="waktu" onchange="orderSubmit()" value="true" {{ ($waktu == 'true' ? 'checked' : '') }} />
                <label for="waktu" class="text-white-50 me-2">Waktu</label>
            </div>
            <div class="ord">
                <input type="checkbox" name="nama" id="nama" onchange="orderSubmit()" value="true"{{ ($nama == 'true' ? 'checked' : '') }} />
                <label for="nama" class="text-white-50 me-2">Nama Produk</label>
            </div>
        </div>
        <select name="type" id="" class="text-white" onchange="orderSubmit()">
            <option value="asc" {{ ($type == 'asc' ? 'selected' : '') }}>Rendah ke Tinggi</option>
            <option value="desc" {{ ($type == 'desc' ? 'selected' : '') }}>Tinggi ke Rendah</option>
        </select>
        <button id="orderBtn" type="submit" style="display: none;"></button>
    </form>
</div>
<div class="items mt-4 d-flex flex-wrap">
    @foreach ($products as $prod)
    <div class="foods mb-3 me-2">
        <a href="/detail/{{$prod->id}}?page=welcome">
            <img src="{{$prod->gambar}}" alt="">
            
            <div class="bg p-2 d-flex justify-content-between align-items-center">
                <h4 class="text-white">{{$prod->nama}}</h4>
                <p class="m-0 blue">Rp {{number_format($prod->harga, 2, ',', '.')}}</p>
            </div>
        </a>
    </div>
    @endforeach
</div>    
<div class="mt-4 d-flex justify-content-center">
    {{ $products->appends(['category' => $category])->links() }}
</div>
@endsection
