@extends('layouts.master')

@section('headers')
<div class="headers">
    <h2 class="text-white">Items Management</h2>
    <p class="text-white-50">{{\Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}</p>
</div>
<div class="navigation d-flex manage-ctg">
    <a href="{{route('category')}}" class="btn text-white manage-ctg d-flex">
        <img src="assets/Filter.png" alt="" class="me-2" />
        <p class="mb-0">Manage Categories</p>
    </a>
</div>
@endsection

@section('body')
@if (Session::has('message'))
<div class="alert alert-success" role="alert">
    {{ Session::get('message') }}
</div>
@endif
<div class="categories mt-2 d-flex">
    <span class="ctg me-5">
        <a href="/product" class="text-decoration-none">
        <p class="{{ (!$ctg ? 'blue' : 'text-white-50') }}">Semua</p>
        @if(!$ctg)
        <div class="bottom"></div>
        @endif
        </a>
    </span>
    @foreach ($categories as $ct)
    <span class="ctg me-5">
        <a href="/product?category={{$ct->nama}}" class="text-decoration-none">
        <p class="{{ ($ctg == $ct->nama ? 'blue' : 'text-white-50') }}">{{$ct->nama}}</p>
        @if($ctg == $ct->nama)
        <div class="bottom"></div>
        @endif
        </a>
    </span>
    @endforeach
</div>
<div class="items mt-4 d-flex flex-wrap">
    <div class="add-item mb-3 me-2">
        <a href="{{route('create')}}" class="text-decoration-none">
        <div class="add blue d-flex flex-column align-items-center justify-content-center">
            <p class="plus">+</p>
            <p>Add new item</p>
        </div>
        </a>
    </div>
    @foreach ($products as $prod)
    <div class="foods mb-3 me-2">
        <a href="detail/{{$prod->id}}?page=product">
        <img src="{{asset('storage/gambar/' . $prod->gambar)}}" alt="" />
        <div class="bg p-2 d-flex justify-content-between align-items-center">
            <h4 class="text-white">{{$prod->nama}}</h4>
            <p class="m-0 blue">Rp {{number_format($prod->harga, 2, ',', '.')}}</p>
        </div>
        </a>
    </div>
    @endforeach
</div>
<div class="mt-4 d-flex justify-content-center">
    {{ $products->appends(['category' => $ctg])->links() }}
</div>
@endsection

@section('button')
<a href="{{route('create')}}" style="background-color: transparent; border: 0; height: fit-content" class="my-carts p-2">
    <div class="my-carts p-2"><h1 class="blue">+</h1></div>
</a>
@endsection