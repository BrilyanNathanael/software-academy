@extends('layouts.master')

@section('headers')
<div class="headers">
    <div class="d-flex align-items-center mb-3">
        <a href="{{route('product')}}" class="back">
        <img src="assets/left-arrow.png" alt="" />
        </a>
        <h2 class="text-white mb-0 ms-2">Add New Item</h2>
    </div>
</div>
@endsection

@section('body')
<form action="/add-item" method="post" class="d-flex flex-column form-add text-white" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
        <label for="productName" class="form-label">Product Name</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="productName" name="nama" />
        @error('nama')
            <p class="text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </p>
        @enderror
    </div>
    <div class="mb-3">
        <label for="productPrice" class="form-label">Product Price</label>
        <input type="number" class="form-control @error('harga') is-invalid @enderror" id="productPrice" name="harga" />
        @error('harga')
            <p class="text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </p>
        @enderror
    </div>
    <div class="mb-3">
        <label for="productCategory" class="form-label">Product Category</label>
        <select name="category" id="productCategory" class="form-select @error('category') is-invalid @enderror">
            @foreach ($categories as $ct)
            <option value="{{$ct->id}}">{{$ct->nama}}</option>    
            @endforeach
        </select>
        @error('category')
            <p class="text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </p>
        @enderror
    </div>
    <div class="mb-3">
        <label for="productImage" class="form-label">Product Image</label>
        <input type="file" class="form-control @error('gambar') is-invalid @enderror" id="productImage" name="gambar" />
        @error('gambar')
            <p class="text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </p>
        @enderror
    </div>
    <div class="mb-3">
        <label for="productDesc" class="form-label">Product Description</label>
        <textarea id="productDesc" cols="30" rows="10" class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi"></textarea>
        @error('deskripsi')
            <p class="text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </p>
        @enderror
    </div>
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-add btn-primary">Submit</button>
    </div>
</form>
@endsection