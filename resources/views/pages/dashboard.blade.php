@extends('layouts.master')

@section('headers')
<div class="headers">
    <h2 class="text-white">Dashboard</h2>
    <p class="text-white-50">{{\Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}</p>
</div>
@endsection

@section('body')
@if (Session::has('message'))
<div class="alert alert-success" role="alert">
    {{ Session::get('message') }}
</div>
@elseif (Session::has('error'))
<div class="alert alert-danger" role="alert">
    {{ Session::get('error') }}
</div>
@endif
<div class="content-dashboard d-flex">
    <div class="desc-table">
        <div class="headers-dashboard d-flex flex-wrap">
        <div class="info p-3 pe-5 me-4 mb-2">
            <div class="d-flex align-items-center">
            <span class="p-2 pt-1 me-2">
                <img src="assets/Coin.png" alt="" />
            </span>
            <h4 class="text-white mt-2">Rp {{number_format($total_pendapatan, 2, ',', '.')}}</h4>
            </div>
            <p class="text-white-50">Total Pendapatan</p>
        </div>
        <div class="info p-3 pe-5 me-4 mb-2">
            <div class="d-flex align-items-center">
            <span class="p-2 pt-1 me-2">
                <img src="assets/Bookmark.png" alt="" />
            </span>
            <h4 class="text-white mt-2">{{$items_count}}</h4>
            </div>
            <p class="text-white-50">Total Item</p>
        </div>
        <div class="info p-3 pe-5 me-4 mb-2">
            <div class="d-flex align-items-center">
            <span class="p-2 pt-1 me-2">
                <img src="assets/user-count.png" alt="" />
            </span>
            <h4 class="text-white mt-2">{{$users_count}}</h4>
            </div>
            <p class="text-white-50">Total Pengguna</p>
        </div>
        </div>
        <div class="table-report p-3 mt-1">
        <h3 class="text-white mt-2 mb-2">Laporan Penarikan</h3>
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">User ID</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Total Penarikan</th>
            </tr>
            </thead>
            <tbody class="text-white-50">
                @foreach ($withdraw as $c)
                <tr>
                    <th>{{$loop->iteration}}</th>
                    <td>{{$c->user_id}}</td>
                    <td>{{$c->created_at->isoFormat('D MMMM Y')}}</td>
                    <td>Rp {{number_format($c->total, 2, ',', '.')}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
    <div class="penarikan p-3">
        <h3 class="text-white">Penarikan Dana</h3>
        <img src="assets/income.png" alt="" />
        <form action="/withdraw" method="POST">
            @csrf
            @method('put')
            <button class="btn btn-primary mt-3 mb-3">Penarikan Pendapatan</button>
        </form>
        <p class="text-white-50">Dengan melakukan penarikan pendapatan Kantin Kejujuran, maka data akan masuk ke dalam laporan penarikan.</p>
    </div>
</div>
@endsection