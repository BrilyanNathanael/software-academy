@extends('layouts.master')

@section('headers')
<div class="headers">
    <div class="d-flex align-items-center mb-3">
        <a href="{{route('product')}}" class="back">
        <img src="assets/left-arrow.png" alt="" />
        </a>
        <h2 class="text-white mb-0 ms-2">Add Category</h2>
    </div>
</div>
@endsection

@section('body')
<form action="/category" method="post" class="d-flex flex-column form-add text-white">
    @csrf
    <div class="mb-3">
        <label for="productName" class="form-label">Category Name</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="productName" name="nama" />
        @error('nama')
            <p class="text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </p>
        @enderror
    </div>
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-add btn-primary">Submit</button>
    </div>
</form>
@endsection