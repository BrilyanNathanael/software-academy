@extends('layouts.master')

@section('headers')
@auth
@if($header == 'product')
<div class="headers">
    <h2 class="text-white">Items Management</h2>
    <p class="text-white-50">{{\Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}</p>
</div>
<div class="navigation d-flex manage-ctg">
    <a href="{{route('category')}}" class="btn text-white manage-ctg d-flex">
        <img src="{{asset('assets/Filter.png')}}" alt="" class="me-2" />
        <p class="mb-0">Manage Categories</p>
    </a>
</div>
@else
<div class="headers">
    <h2 class="text-white">{{Auth::user()->user_id}}</h2>
    <p class="text-white-50">{{\Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}</p>
</div>
<div class="navigation">
    <form action="/home" method="get">
        <div class="mb-3 group d-flex align-items-end">
            <img src="{{asset('assets/search.png')}}" alt="" class="icon icon-search" onclick="searchClick();" />
            <input type="text" id="search" class="form-control input" name="search" placeholder="Cari makanan, minum.." />
            <button type="submit" style="display: none;" id="searchBtn"></button>
        </div>
    </form>
    <button onclick="$('.cart-payment').toggleClass('active');$('.overlay').toggleClass('active');" style="background-color: transparent; border: 0; height: fit-content" class="mt-1 ms-4 listCart">
        <img src="{{asset('assets/shopping-cart.png')}}" alt="" width="30" height="30" />
    </button>
</div>
@endif
@else
<div class="headers">
    <h2 class="text-white">Hello, Anonymous!</h2>
    <p class="text-white-50">{{\Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}</p>
</div>
<div class="navigation authentication">
    <a href="{{route('login')}}" class="text-decoration-none me-3">Login</a>
    <a href="{{route('register')}}" class="text-decoration-none">Register</a>
</div>
@endauth
@endsection

@section('body')
<div class="detail p-1">
    <div class="d-flex">
        <a href="{{route($header)}}" class="back me-3 mt-2">
        <img src="{{asset('assets/left-arrow.png')}}" alt="" />
        </a>
        <img src="{{asset('storage/gambar/' . $prods->gambar)}}" alt="" />
    </div>
    <div class="descriptions">
        <h2 class="text-white mt-3">{{$prods->nama}}</h2>
        <hr />
        <p class="text-white-50">
            {{$prods->deskripsi}}
        </p>
        <div class="add-cart d-flex align-items-center">
        <h3 class="blue">Rp {{number_format($prods->harga, 2, ',', '.')}}</h3>
        @if($header == 'home')
        <form action="/add-to-cart" method="post">
            @csrf
            <input type="hidden" name="user_id" value="{{Auth::user()->user_id}}">
            <input type="hidden" name="product_id" value="{{$prods->id}}">
            <button class="p-2 ms-4" type="submit">
                <img src="{{asset('assets/add-cart.png')}}" alt="" />
            </button>
        </form>
        @endif
        </div>
    </div>
</div>
@endsection

@section('button')
@if($header == 'home')
<button style="background-color: transparent; border: 0; height: fit-content" class="my-carts p-2" onclick="cart()">
    <div class="my-carts p-2">
        <img src="{{asset('assets/shopping-cart.png')}}" alt="" width="30" height="30" />
    </div>
</button>
@endif
@endsection

@section('overlay')
@if($header == 'home')
<div class="overlay"></div>
    <div class="cart-payment p-3">
    <div class="d-flex justify-content-end">
        <button class="close" onclick="$('.cart-payment').toggleClass('active');$('.overlay').toggleClass('active');">
        <img src="{{asset('assets/close.png')}}" alt="" />
        </button>
    </div>
    <h2 class="text-white">My Cart</h2>
    <hr />
    <div class="carts mb-3">
        <?php $total = 0; ?>
        @foreach ($carts as $c)
        <div class="cart-item d-flex align-items-center justify-content-between ps-3 pe-3 mb-3">
            <div class="items d-flex">
            <img src="{{asset('storage/gambar/' . $c->gambar)}}" alt="" />
            <div class="desc">
                <h6 class="text-white ms-3">{{$c->nama}}</h6>
                <p class="text-white-50 ms-3 m-0">Rp {{number_format($c->harga, 2, ',', '.')}}</p>
                <?php $total += $c->harga; ?>
            </div>
            </div>
            <div class="menu-cart d-flex align-items-center">
                <form action="/remove-cart/{{$c->cart_id}}" method="post">
                    @csrf
                    @method('delete')
                    <button class="ms-2" type="submit">
                        <img src="{{asset('assets/trash.png')}}" alt="" class="trash" />
                    </button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
    <hr />
    <div class="prices d-flex justify-content-between">
    <p class="m-0 text-white-50">Total Price</p>
    <p class="m-0 text-white">Rp {{number_format($total, 2, ',', '.')}}</p>
    </div>
    <button class="btn mt-3 checkout btn-primary"><b>Checkout Payment</b></button>
</div>
@endif
@endsection