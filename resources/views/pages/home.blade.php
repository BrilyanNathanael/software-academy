@extends('layouts.master')

@section('headers')
<div class="headers">
    <h2 class="text-white">{{Auth::user()->user_id}}</h2>
    <p class="text-white-50">{{\Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}</p>
</div>
<div class="navigation">
    <form action="/home" method="get">
        <div class="mb-3 group d-flex align-items-end">
            <img src="assets/search.png" alt="" class="icon icon-search" onclick="searchClick();" />
            <input type="text" id="search" class="form-control input" name="search" placeholder="Cari makanan, minum.." value="{{ ($search ? $search : '') }}" />
            <button type="submit" style="display: none;" id="searchBtn"></button>
        </div>
    </form>
    <button style="background-color: transparent; border: 0; height: fit-content" class="mt-1 ms-4 listCart" onclick="cart()">
        <img src="assets/shopping-cart.png" alt="" width="30" height="30" />
    </button>
</div>
@endsection

@section('body')
@if (Session::has('message'))
<div class="alert alert-success" role="alert">
    {{ Session::get('message') }}
</div>
@elseif (Session::has('error'))
<div class="alert alert-danger" role="alert">
    {{ Session::get('error') }}
</div>
@endif
<div class="categories mt-2 d-flex">
    <span class="ctg me-5">
        <a href="/home" class="text-decoration-none">
        <p class="{{ (!$category ? 'blue' : 'text-white-50') }}">Semua</p>
        @if(!$category)
        <div class="bottom"></div>
        @endif
        </a>
    </span>
    @foreach ($categories as $ct)
    <span class="ctg me-5">
        <a href="/home?category={{$ct->nama}}" class="text-decoration-none">
        <p class="{{ ($category == $ct->nama ? 'blue' : 'text-white-50') }}">{{$ct->nama}}</p>
        @if($category == $ct->nama)
        <div class="bottom"></div>
        @endif
        </a>
    </span>
    @endforeach
</div>
<div class="orders pe-4">
    <form action="/home" method="get" class="d-flex justify-content-between">
        <input type="hidden" name="search" value="{{ $search }}">
        <input type="hidden" name="category" value="{{ $category }}">
        <div class="order-by">
            <p class="text-white mt-2 mb-0">Order By:</p>
            <div class="ord">
                <input type="checkbox" name="waktu" id="waktu" onchange="orderSubmit()" value="true" {{ ($waktu == 'true' ? 'checked' : '') }} />
                <label for="waktu" class="text-white-50 me-2">Waktu</label>
            </div>
            <div class="ord">
                <input type="checkbox" name="nama" id="nama" onchange="orderSubmit()" value="true"{{ ($nama == 'true' ? 'checked' : '') }} />
                <label for="nama" class="text-white-50 me-2">Nama Produk</label>
            </div>
        </div>
        <select name="type" id="" class="text-white" onchange="orderSubmit()">
            <option value="asc" {{ ($type == 'asc' ? 'selected' : '') }}>Rendah ke Tinggi</option>
            <option value="desc" {{ ($type == 'desc' ? 'selected' : '') }}>Tinggi ke Rendah</option>
        </select>
        <button id="orderBtn" type="submit" style="display: none;"></button>
    </form>
</div>
<div class="items mt-4 d-flex flex-wrap">
    @foreach ($products as $prod)
    <div class="foods mb-3 me-2">
        <a href="/detail/{{$prod->id}}?page=home">
            <img src="{{asset('storage/gambar/' . $prod->gambar)}}" alt="" />
            <div class="bg p-2 d-flex justify-content-between align-items-center">
                <h4 class="text-white">{{$prod->nama}}</h4>
                <p class="m-0 blue">Rp {{number_format($prod->harga, 2, ',', '.')}}</p>
            </div>
        </a>
    </div>
    @endforeach
</div>    
<div class="mt-4 d-flex justify-content-center">
    {{ $products->appends(['category' => $category])->links() }}
</div>
@endsection

@section('button')
<button style="background-color: transparent; border: 0; height: fit-content" class="my-carts p-2" onclick="cart()">
    <div class="my-carts p-2">
        <img src="assets/shopping-cart.png" alt="" width="30" height="30" />
    </div>
</button>
@endsection

@section('overlay')
<div class="overlay"></div>
    <div class="cart-payment p-3">
    <div class="d-flex justify-content-end">
        <button class="close" onclick="cart()">
        <img src="assets/close.png" alt="" />
        </button>
    </div>
    <h2 class="text-white">My Cart</h2>
    <hr />
    <div class="carts mb-3">
        <?php $total = 0; ?>
        @foreach ($carts as $c)
        <div class="cart-item d-flex align-items-center justify-content-between ps-3 pe-3 mb-3">
            <div class="items d-flex">
            <img src="{{asset('storage/gambar/' . $c->gambar)}}" alt="" />
            <div class="desc">
                <h6 class="text-white ms-3">{{$c->nama}}</h6>
                <p class="text-white-50 ms-3 m-0">Rp {{number_format($c->harga, 2, ',', '.')}}</p>
                <?php $total += $c->harga; ?>
            </div>
            </div>
            <div class="menu-cart d-flex align-items-center">
                <form action="/remove-cart/{{$c->cart_id}}" method="POST">
                    @csrf
                    @method('delete')
                    <button type="submit" class="ms-2">
                        <img src="assets/trash.png" alt="" class="trash" />
                    </button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
    <hr />
    <div class="prices d-flex justify-content-between">
    <p class="m-0 text-white-50">Total Price</p>
    <p class="m-0 text-white">Rp {{number_format($total, 2, ',', '.')}}</p>
    </div>
    <form action="/checkout" method="post">
        @csrf
        <button class="btn mt-3 checkout btn-primary" type="submit"><b>Checkout Payment</b></button>
    </form>
</div>
@endsection
