@extends('layouts.signed')

@section('sign') Sign Up @endsection

@section('formSign')
<form method="POST" action="{{ route('register') }}">
    @csrf
    <p class="text-white-50 mb-0">User ID harus terdiri dari 5 digit angka dari 0-9. Dua digit terakhir merupakan penjumlahan tiga digit pertama.</p>
    <p class="text-white-50 mb-0">Contoh :</p>
    <ul class="text-white">
        <li>45615 (4+5+6 = 15)</li>
        <li>41207 (4+1+2 = 7)</li>
    </ul>
    <div class="mb-3 group">
        <img src="assets/user.png" alt="" class="icon" />
        <input type="text" class="form-control input @error('user_id') is-invalid @enderror" name="user_id" placeholder="User ID" />
    </div>
    @error('user_id')
        <p class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </p>
    @enderror

    <div class="mb-3 group">
        <img src="assets/padlock.png" alt="" class="icon" />
        <input type="password" class="form-control input @error('password') is-invalid @enderror" name="password" placeholder="Password" />
    </div>
    @error('password')
        <p class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </p>
    @enderror

    <div class="mb-3 group">
        <img src="assets/padlock.png" alt="" class="icon" />
        <input type="password" class="form-control input" name="password_confirmation" placeholder="Password Confirmation" />
    </div>
    <p class="text-white-50">
        Sudah punya akun?<a href="{{route('login')}}" class="text-decoration-none ms-1"><span class="blue">Sign In</span></a>
    </p>
    <button type="submit" class="btn btn-primary text-white" style="width: 100%; background-color: rgb(52, 127, 201)">Submit</button>
</form>
@endsection