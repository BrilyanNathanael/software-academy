@extends('layouts.signed')

@section('sign') Sign In @endsection

@section('formSign')
<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="mb-3 group">
        <img src="assets/user.png" alt="" class="icon" />
        <input type="text" class="form-control input @error('user_id') is-invalid @enderror" name="user_id" placeholder="User ID" />
    </div>
    @error('user_id')
        <p class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </p>
    @enderror
    
    <div class="mb-3 group">
        <img src="assets/padlock.png" alt="" class="icon" />
        <input type="password" class="form-control input @error('password') is-invalid @enderror" name="password" placeholder="Password" />
    </div>
    @error('password')
        <p class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </p>
    @enderror

    <p class="text-white-50">
        Tidak punya akun?<a href="{{route('register')}}" class="text-decoration-none ms-1"><span class="blue">Sign Up</span></a>
    </p>
    <button type="submit" class="btn btn-primary text-white" style="width: 100%; background-color: rgb(52, 127, 201)">Submit</button>
</form>
@endsection