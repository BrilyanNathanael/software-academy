<div class="sidebar p-3 d-flex flex-column align-items-center justify-content-around">
    <a href="{{route('home')}}">
        <img src="{{(Request::is('home') ? asset('assets/home-active.png') : asset('assets/home.png'))}}" alt="" />
    </a>
    @auth
    <a href="{{route('dashboard')}}">
        <img src="{{(Request::is('dashboard') ? asset('assets/dashboard-active.png') : asset('assets/dashboard.png'))}}" alt="" />
    </a>
    <a href="{{route('product')}}">
        <img src="{{((Request::is('product') || Request::is('category') || Request::is('add-item')) ? asset('assets/prod-active.png') : asset('assets/prod.png'))}}" alt="" />
    </a>
    <a onclick="logout()">
        <img src="{{asset('assets/logout.png')}}" alt="" />
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
    @endauth
</div>