<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>@yield('sign')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('style.css')}}" />
  </head>
  <body>
    <div class="d-flex">
      <div class="background flex-column justify-content-center align-items-center">
        <h1 class="text-white text-center mb-2"><b>Halo! Selamat datang di Kantin Kejujuran</b></h1>
        <p class="text-white-50">Berani jujur itu hebat!</p>
        <img src="assets/children-canteen.png" alt="" />
      </div>
      <div class="content p-4">
        <div class="header-content">
          <h5 class="text-white">
            <b><span class="blue">Kantin</span> Kejujuran</b>
          </h5>
        </div>
        <div class="body-content flex-column justify-content-center align-items-center">
          <h3 class="text-white mb-4">
            <b>
                @yield('sign')
            </b>
        </h3>
        @yield('formSign')
        </div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
  </body>
</html>
