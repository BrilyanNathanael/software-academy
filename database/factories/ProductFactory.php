<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    function random_pic($dir)
    {
        $files = glob($dir . '/*.*');
        $file = array_rand($files);
        return $files[$file];
    }

    public function definition()
    {
        return [
            'category_id' => $this->faker->numberBetween(1, 5),
            'nama' => $this->faker->word,
            'harga' => $this->faker->numberBetween(25000, 30000),
            'gambar' => $this->faker->imageUrl(640,480),
            'deskripsi' => $this->faker->paragraph()
        ];
    }
}
