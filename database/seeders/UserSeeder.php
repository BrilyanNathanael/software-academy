<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'user_id' => '45211',
            'password' => Hash::make('password123'),
        ]);
        User::create([
            'user_id' => '34714',
            'password' => Hash::make('password123'),
        ]);
        User::create([
            'user_id' => '55212',
            'password' => Hash::make('password123'),
        ]);
        User::create([
            'user_id' => '86216',
            'password' => Hash::make('password123'),
        ]);
        User::create([
            'user_id' => '98623',
            'password' => Hash::make('password123'),
        ]);
    }
}
