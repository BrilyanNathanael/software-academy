<h1>
Kantin Kejujuran
</h1>
<p>
Project Kantin Kejujuran merupakan website aplikasi, dimana pengguna dapat melakukan penjualan dan pembelian, serta melakukan penarikan dari hasil pendapatan yang diterima.
</p>

<b>
Project ini menggunakan Laravel 9
</b>
<p>
Prosedur instalasi project ini:
<ol>
<li>
Copy file .env-example lalu rename menjadi .env
</li>
<li>
Setting database
</li>
<li>
Jalankan perintah "composer install" pada terminal
</li>
<li>
php artisan key:generate
</li>
<li>
php artisan migrate
</li>
</ol>
</p>

