<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    public function index(Request $request)
    {
        $ctg= $request->input('category');

        if($ctg)
        {
            $category_id = Category::where('nama', $ctg)->first();

            $products = Product::select('*')
                        ->where('category_id', $category_id->id)
                        ->orderBy('created_at', 'asc')
                        ->paginate(7);
        }
        else
        {
            $products = Product::paginate(7);
        }

        $categories = Category::all();
        return view('pages.product', compact('ctg', 'products', 'categories'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('pages.add-item', compact('categories'));
    }

    public function store(Request $request)
    {
        $message = array(
            'nama.required' => 'Nama harus diisi.',
            'category.required' => 'Category harus diisi.',
            'harga.required' => 'Harga harus diisi.',
            'harga.integer' => 'Harga harus terdiri dari angka.',
            'gambar.required' => 'Gambar harus diisi.',
            'deskripsi.required' => 'Deskripsi harus diisi.',
            'deskripsi.min' => 'Min. 10 karakter.',
        );
        $request->validate([
            'nama' => 'required',
            'category' => 'required',
            'harga' => 'required|integer',
            'gambar' => 'required',
            'deskripsi' => 'required|min:10',
        ],$message);
        
        $extension = $request->file('gambar')->getClientOriginalExtension();
        $newfilename = time() . '.' . $extension;
        $path = $request->file('gambar')->storeAs('public/gambar', $newfilename);

        Product::create([
            'category_id' => $request->category,
            'nama' => $request->nama,
            'harga' => $request->harga,
            'gambar' => $newfilename,
            'deskripsi' => $request->deskripsi,
        ]);

        return redirect('product')->with('message', 'Sukses Menambahkan Produk!');
    }

    public function show($id, Request $request)
    {
        if($request->input('page') == 'product'){
            $header = "product";
        }
        else if($request->input('page') == 'home'){
            $header = "home";
        }
        else{
            $header = "welcome";
        }

        $prods = Product::where('id',$id)->first();
        if(Auth::user())
        {
            $user = Auth::user()->user_id;
    
            $carts = Cart::join('products', 'carts.product_id', '=', 'products.id')
            ->where('carts.user_id', $user)
            ->get(['carts.id as cart_id', 'carts.*', 'products.*']);

            return view('pages.detail', compact('carts', 'header', 'prods'));
        }

        return view('pages.detail', compact('header', 'prods'));

    }

}
