<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Rules\UserIdRule;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $message = array(
            'user_id.required' => 'User Id harus diisi.',
            'user_id.unique' => 'User Id sudah pernah tersimpan.',
            'user_id.numeric' => 'User Id harus terdiri dari angka.',
            'user_id.digits_between' => 'User Id harus terdiri dari 5 digit.',
            'password.required' => 'Password harus diisi.',
            'password.min' => 'Min: 8 karakter.',
            'password.confirmed' => 'Konfirmasi Password tidak sesuai.',
        );

        return Validator::make($data, [
            'user_id' => ['required', 'numeric', 'digits_between:5,5', 'unique:users,user_id', new UserIdRule()],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], $message);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'user_id' => $data['user_id'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
