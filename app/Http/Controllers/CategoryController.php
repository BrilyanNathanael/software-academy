<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return view('pages.category');
    }
    
    public function store(Request $request)
    {
        $message = array(
            'nama.required' => 'Nama harus diisi.',
            'nama.unique' => 'Nama sudah pernah tersimpan.',
        );
        $request->validate([
            'nama' => 'required|unique:categories,nama',
        ],$message);

        Category::create([
            'nama' => $request->nama,
        ]);

        return redirect('/product')->with('message', 'Sukses Menambahkan Kategori!');;
    }
}
