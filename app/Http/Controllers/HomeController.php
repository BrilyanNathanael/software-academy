<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        $waktu = $request->input('waktu');
        $nama = $request->input('nama');
        $type = $request->input('type');
        $category = $request->input('category');

        $user = Auth::user()->user_id;
        $carts = Cart::join('products', 'carts.product_id', '=', 'products.id')
                ->where('carts.user_id', $user)
                ->get(['carts.id as cart_id', 'carts.*', 'products.*']);

        if($category)
        {
            $category_id = Category::where('nama', $category)->first();

            if(($waktu == 'true' || $type) && !$nama)
            {
                $products = Product::select('*')
                            ->where('category_id', $category_id->id)
                            ->orderBy('created_at', $type)
                            ->paginate(8);
            }
            else if($nama == 'true' && !$waktu)
            {
                $products = Product::select('*')
                            ->where('category_id', $category_id->id)
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else if($waktu == 'true' && $waktu == 'true')
            {
                $products = Product::select('*')
                            ->where('category_id', $category_id->id)
                            ->orderBy('created_at', $type)
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else
            {
                $products = Product::select('*')
                            ->where('category_id', $category_id->id)
                            ->orderBy('created_at', 'asc')
                            ->paginate(8);
            }
        }
        else if($search)
        {
            if(($waktu == 'true' || $type) && !$nama)
            {
                $products = Product::select('*')
                            ->where('nama', 'like', '%'.$search.'%')
                            ->orderBy('created_at', $type)
                            ->paginate(8);
            }
            else if($nama == 'true' && !$waktu)
            {
                $products = Product::select('*')
                            ->where('nama', 'like', '%'.$search.'%')
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else if($waktu == 'true' && $waktu == 'true')
            {
                $products = Product::select('*')
                            ->where('nama', 'like', '%'.$search.'%')
                            ->orderBy('created_at', $type)
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else
            {
                $products = Product::select('*')
                            ->where('nama', 'like', '%'.$search.'%')
                            ->orderBy('created_at', 'asc')
                            ->paginate(8);
            }
        }
        else
        {
            if(($waktu == 'true' || $type) && !$nama)
            {
                $products = Product::select('*')
                            ->orderBy('created_at', $type)
                            ->paginate(8);
            }
            else if($nama == 'true' && !$waktu)
            {
                $products = Product::select('*')
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else if($waktu == 'true' && $waktu == 'true')
            {
                $products = Product::select('*')
                            ->orderBy('created_at', $type)
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else
            {
                $products = Product::select('*')
                            ->orderBy('created_at', 'asc')
                            ->paginate(8);
            }
        }

        $categories = Category::all();
        return view('pages.home', compact('category', 'nama', 'waktu', 'type', 'search', 'carts', 'products', 'categories'));
    }
}
