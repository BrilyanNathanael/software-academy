<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Checkout;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $checkout = Checkout::where('is_withdraw', 'N')->get();
        $total_pendapatan = 0;
        foreach($checkout as $c)
        {
            $total_pendapatan += $c->total;
        }

        $withdraw = Checkout::where('is_withdraw', 'Y')->get();
        $items_count = Product::count();
        $users_count = User::count();

        return view('pages.dashboard', compact('withdraw', 'total_pendapatan', 'items_count', 'users_count'));
    }

    public function withdraw()
    {
        $checkout = Checkout::where('is_withdraw', 'N')->get();
        $checkoutCount = Checkout::where('is_withdraw', 'N')->count();

        if($checkoutCount > 0)
        {
            foreach($checkout as $c)
            {
                $c->update([
                    'is_withdraw' => 'Y',
                ]);
                
            }
            return redirect('dashboard')->with('message', 'Sukses Melakukan Penarikan Pendapatan!');
        }
        else
        {
            return redirect('dashboard')->with('error', 'Gagal Melakukan Penarikan Pendapatan!');
        }


    }

    public function welcome(Request $request)
    {
        if(Auth::user())
        {
            return redirect('/home');
        }
        
        $waktu = $request->input('waktu');
        $nama = $request->input('nama');
        $type = $request->input('type');
        $category = $request->input('category');

        if($category)
        {
            $category_id = Category::where('nama', $category)->first();

            if(($waktu == 'true' || $type) && !$nama)
            {
                $products = Product::select('*')
                            ->where('category_id', $category_id->id)
                            ->orderBy('created_at', $type)
                            ->paginate(8);
            }
            else if($nama == 'true' && !$waktu)
            {
                $products = Product::select('*')
                            ->where('category_id', $category_id->id)
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else if($waktu == 'true' && $waktu == 'true')
            {
                $products = Product::select('*')
                            ->where('category_id', $category_id->id)
                            ->orderBy('created_at', $type)
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else
            {
                $products = Product::select('*')
                            ->where('category_id', $category_id->id)
                            ->orderBy('created_at', 'asc')
                            ->paginate(8);
            }
        }
        else
        {
            if(($waktu == 'true' || $type) && !$nama)
            {
                $products = Product::select('*')
                            ->orderBy('created_at', $type)
                            ->paginate(8);
            }
            else if($nama == 'true' && !$waktu)
            {
                $products = Product::select('*')
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else if($waktu == 'true' && $waktu == 'true')
            {
                $products = Product::select('*')
                            ->orderBy('created_at', $type)
                            ->orderBy('nama', $type)
                            ->paginate(8);
            }
            else
            {
                $products = Product::select('*')
                            ->orderBy('created_at', 'asc')
                            ->paginate(8);
            }
        }

        $categories = Category::all();
        return view('pages.welcome', compact('category', 'nama', 'waktu', 'type', 'products', 'categories'));
    }
}
