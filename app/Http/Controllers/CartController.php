<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Checkout;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    public function store(Request $request)
    {
        $cart = Cart::where('user_id', $request->user_id)
                ->where('product_id', $request->product_id)
                ->first();
        
        if(!$cart)
        {
            Cart::create([
                'user_id' => $request->user_id,
                'product_id' => $request->product_id,
            ]);

            return redirect('home')->with('message', 'Sukses Memasukkan Produk ke Dalam Keranjang!');
        }
        return redirect('home')->with('error', 'Produk sudah di dalam keranjang!');

    }

    public function destroy($id)
    {
        Cart::destroy($id);
        return redirect('home');
    }

    public function checkout()
    {
        $user = Auth::user()->user_id;
        $carts = Cart::join('products', 'carts.product_id', '=', 'products.id')
        ->where('carts.user_id', $user)
        ->get(['carts.id as cart_id', 'carts.*', 'products.*']);

        $total = 0;
        foreach($carts as $c)
        {
            $total += $c->harga;
            Product::where('id', $c->id)->delete();
        }

        Checkout::create([
            'user_id' => $user,
            'total' => $total,
            'is_withdraw' => 'N',
        ]);

        return redirect('home')->with('message', 'Sukses Melakukan Checkout Payment!');
    }
}
