<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UserIdRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $split_characters = str_split($value);
        $result = ($split_characters[3] == 0) ? $split_characters[4] : $split_characters[3] . $split_characters[4];

        if((int)$split_characters[0] + (int)$split_characters[1] + (int)$split_characters[2] == $result){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'User Id tidak sesuai dengan ketentuan.';
    }
}
